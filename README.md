# Cpp201
follow the order for a better experience.

For visual studio users: exec [cmake -G "Visual Studio 15 2017"] or use the cmake vs extention

## KISS
Introduction au "Keep it simple" (lambda auto etc...)
## AbstractionLevel
Contrôler (private) et utiliser différents niveaux d'abstraction
## Templates
Présentation des Template basiques et variadiques
## StrongTyping
Utilisation du typage fort
## Ownership
Définir la possession et le partage d'une donnée
