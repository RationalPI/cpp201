#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

struct BadCar{
	float speed=0;
	const float mass=100;

	struct FuelType{
		const float energy=100;
	}fuelType;

	struct Engine{
		bool runing=false;
		const float consumtion=1;//!< liter per minutes
		const float efficiency=0.45;
	}engine;

	struct FuelTank{
		float fuelQuantity=100;//!< liter
	}fuelTank;

	struct IgnitionModule{
		bool inWorkingOrder=true;
	}ignitionModule;
};

struct GoodCar{
	enum class RuntimeError{
		noMoreFuel,
	};

	void start(){
		if(ignitionModule.inWorkingOrder&&fuelTank.fuelQuantity>0){
			engine.runing=true;
		}
	}

	bool hasFuel(){return fuelTank.fuelQuantity>0;}

	//! @param time in seconds
	//! throw if there is no fuel
	void runFor(float time){
		if(fuelTank.fuelQuantity<=0){
			throw RuntimeError::noMoreFuel;
		}
		if(engine.runing){
			speed+=mass*engine.burnFuel(time,fuelType);
			fuelTank.fuelQuantity-=engine.consumtion/60*time;
		}
	}

private:
	float speed=0;
	const float mass=100;

	struct FuelType{
		const float energy=100;//! energy per liter
	}fuelType;

	struct Engine{
		bool runing=false;
		//! return energi per fuel burnt
		float burnFuel(float time,FuelType f){return f.energy*efficiency*time*consumtion;}
		const float consumtion=1;//!< liter per minutes
	private:
		const float efficiency=0.45;
	}engine;

	struct FuelTank{
		float fuelQuantity=100;//!< liter
	}fuelTank;

	struct IgnitionModule{
		bool inWorkingOrder=true;
	}ignitionModule;
};

int main(){

	/*run a car until it has no more fuel (BAD)*/{
		BadCar badCar;
		//making the care go forward

		//start the car engine
		if(badCar.ignitionModule.inWorkingOrder&&badCar.fuelTank.fuelQuantity>0){
			badCar.engine.runing=true;
		}

		//run the car until no fuel
		while (badCar.fuelTank.fuelQuantity>0) {
			constexpr auto timeStep=1;//!< seconds
			std::this_thread::sleep_for (std::chrono::seconds(timeStep));
			if(badCar.fuelTank.fuelQuantity<=0){
				badCar.engine.runing=false;
			}
			if(badCar.engine.runing){
				badCar.speed+=badCar.mass*badCar.fuelType.energy*timeStep/badCar.engine.efficiency;
				badCar.fuelTank.fuelQuantity-=badCar.engine.consumtion/60*timeStep;
			}
		}
	}

	/*run a car until it has no more fuel (BETTER)*/{
		GoodCar goodCar;
		goodCar.start();
		while(goodCar.hasFuel()){
			constexpr auto timeStep=1;//!< seconds
			std::this_thread::sleep_for (std::chrono::seconds(timeStep));
			goodCar.runFor(timeStep);
		}
	}

	// in the first exemple you need a physisit to make the car run
	// in the second exemple you need a physisit to help you make good car model
	// then thest is then everybody can make the car run

	// keep the data to knowledg to how needs it and
	// dont mix hight level operation with low level ones

	return 0;
}
