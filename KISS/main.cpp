#include <functional>
#include <vector>
#include <array>
#include <tuple>
#include <maybe.h>
#include <string>

namespace bonus {
	/*for return that can fail*/
	bool f2(int&);/*BAD*/
	std::shared_ptr<int> f3();/*BAD*/
	Maybe/*std::optional if c++ 17*/<int> f1();/*BETTER*/
	}

int main(){

	/*fonction pointer / lambda variable*/{
		int(*f1)(int)					= [](int ){return 0;};/*very BAD*/
		std::function<int(int)> f2 = [](int ){return 0;};/*BETTER*/
		auto f3							= [](int ){return 0;};/*even BETTER but can't be passed around*/
	}

	/*misc types*/{
		// arrays
		int a1[42];/*BAD*/
		std::array<int,42> a2;/*BETTER*/

		// dynamic size buffer
		int *b1 = new int[99];/*BAD*/
		std::vector<int> b2; b2.resize(99);/*BETTER*/

		//string of char
		const char* s1="hello";/*BAD*/
		auto s2="hello";/*BETTER*/
		const std::string s3="hello";/*BETTER*/
	}

	/*container traversal*/{
		std::vector<int> v;

		for (size_t i=0;i<v.size();i++) /*BAD*/{
			auto& vElement=v[i];
		}
		for (auto i=0;i<v.size();i++) /*BETTER but BAD*/{
			auto& vElement=v[i];
		}
		for (std::vector<int>::iterator i=v.begin();i!=v.end();i++) /*BAD*/{
			auto& vElement=*i;
		}
		for (auto i=v.begin();i!=v.end();i++) /*BETTER but BAD*/{
			auto& vElement=*i;
		}
		for (auto& vElement:v) /*BETTER*/{

		}
	}

	/*comparaison*/{
		struct Data{
			int a,b,c;
		};

		auto greater1=[](const Data& a,const Data& b)/*BAD*/{
			if(a.a>b.a){
				return true;
			}else if(a.b>b.b){
				return true;
			}else if(a.c>b.c){
				return true;
			}else{
				return false;
			}
		};
		auto greater2=[](const Data& a,const Data& b)/*BETTER*/{
			return std::tie(a.a,a.b,a.c)>std::tie(b.a,b.b,b.c);
		};

	}

	/*bonus*/{
		enum Bad/*BAD*/{
			oneThing=0,
		};
		enum antherKindOfBad/*BAD*/{
			totalyAnotherThing=0,
		};
		// thes two compare to the same ting
		static_assert(oneThing==totalyAnotherThing,"");

		enum class Better/*BETTER*/{
			aThing=0,//!< can't be confused with a plain integer 0
		};

		//this won't even compile: static_assert(Better::aThing!=0,"not equal");

	}

	return 0;
}


