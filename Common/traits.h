#pragma once

#include <type_traits>
#include <tuple>
#include <vector>
#include <memory>
template <class U, class T>
struct is_explicitly_convertible{
  enum {value = std::is_constructible<T, U>::value && !std::is_convertible<U, T>::value};
};

template <class U, class T>
struct is_implicitly_convertible{
  enum {value = std::is_convertible<U, T>::value && !is_explicitly_convertible<U, T>::value};
};

//! Tell if a type is a specialization of a template
struct is_instantiation{
template<template<typename...> class TT, typename T >
struct of : std::false_type{};

template<template<typename...> class TT, typename ...T >
struct of<TT, TT<T...>> : std::true_type{};
};


//####################
//### is_complete ####
//####################
struct char256 { char x[256]; };
template <typename T> char256 is_complete_helper(int(*)[sizeof(T)]);
template <typename> char is_complete_helper(...);
template <typename T> struct is_complete{
	 enum { value = sizeof(is_complete_helper<T>(0)) != 1 };
};

template<typename ...Contained>
class TypeList{
	//! template declaration of the basique struct
	template<typename Tested, typename Tuple_T>
	struct tuple_has_type;

	//! template specialisation if the testedType is not in front of the tuple
	//! and there is something to unpack after : UNPACKING
	template<typename Tested, typename OtherT, typename ...Rest>
	struct tuple_has_type<Tested, std::tuple<OtherT, Rest...>> : tuple_has_type<Tested, std::tuple<Rest...>> {};

	//! template specialisation if the testedType is in front of the tuple : HAS_TYPE
	template<typename Tested, typename ...Rest>
	struct tuple_has_type<Tested, std::tuple<Tested, Rest...>> : std::true_type {};

	//! template specialisation if there is not more type to unpack in the tuple : NOT_HAS_TYPE
	template<typename Tested>
	struct tuple_has_type<Tested, std::tuple<>> : std::false_type {};

public:
	using CONTAINED=std::tuple<Contained...>;

	//! Tell if the type T is contained in Parameter pack (Contained)
	template<typename T>
    struct contains{
		static constexpr bool value = tuple_has_type<T, CONTAINED>::type::value;
	};

    struct empty{
        static constexpr bool value = std::tuple_size<CONTAINED>::value == 0;
    };

	//! assert that the type T is contained in Parameter pack (Contained)
	template<typename T>
	static void assertContained(){
		static_assert (contains<T>::value, "Type not contained");
	}

	//! return a vector of shared ptr. Each shared ptr is a instantiation of one type in parameter pack (Contained)
	template<typename T>
	static std::vector<std::shared_ptr<T>> instantiateAllListedTypesAsDerivateOf(){
		return std::vector<std::shared_ptr<T>>(std::initializer_list<std::shared_ptr<T>>({std::make_shared<Contained>()...}));
	}
};
