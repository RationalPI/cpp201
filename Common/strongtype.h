#pragma once

#include <memory>
#include <string>

// allow to make two types that are inheriting from a same type and that are not convertible from one to another

//! The strong_type struct encapsulate and isolate types
template <typename T,typename tag>
struct strong_type{
    strong_type():value_(){}
    //! implicit from T construction
    strong_type(T t):value_(std::move(t)){}
    //! deleted between any strong_type conversion
    template <typename T2,typename tag2>
    explicit strong_type(strong_type< T2, tag2> const&)=delete;
    //! implicit T convertion
    explicit operator T() const {return value_;}
    bool operator ==(const strong_type<T,tag>& other) const {return value_==other.value_;}
    bool operator !=(const strong_type<T,tag>& other) const {return value_!=other.value_;}
    bool operator <(const strong_type<T,tag>& other) const {return value_<other.value_;}

    T& val(){return value_;}
    const T& val()const{return value_;}
    T* get(){return &value_;}
    const T* get()const{return &value_;}
private:
    T value_;
};

/* to use :
#include <API_Base/safetype.h>
strong_type(newTypName,underlying type,export dierective)
*/

#define NO_EXPORT /*empty ewport directive*/

#define FLAGED(val)val ## _TypeFlag

#define strong_type(name, T, EXPORT_DIRECTIVE)\
    struct EXPORT_DIRECTIVE FLAGED(name){};\
    struct EXPORT_DIRECTIVE name :public strong_type<T,FLAGED(name)>{\
        using strong_type::strong_type;\
        using BASE=T;\
    };



