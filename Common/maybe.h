#pragma once
#include <assert.h>
#include <memory>
#include "traits.h"

#define CPP99 199711L
#define CPP11 201103L
#define CPP14 201402L
#define CPP17 201703L
#define CPP20 202002L
static_assert (__cplusplus < CPP17,"to future maintainers: if using C++17+ port this code and usages to std::optional<>");

template<class T>
//! encapsulate what could be "a pointer that can be pointing to nothing", this is equal to c++ 17 std::optional (as we are in c++11 for the time)
struct MaybeBase{
	using value_type=T;
	inline virtual ~MaybeBase(){}

	inline MaybeBase(){}

	inline explicit MaybeBase(const T& data):value(std::make_shared<T>(data)){}
	inline explicit MaybeBase(T&& data):value(std::make_shared<T>(std::move(data))){}

	inline MaybeBase(const MaybeBase& other):value(other.full()?std::make_shared<T>(*other.value):nullptr){}/*copy constr*/
	inline MaybeBase(MaybeBase&& other):value(other.full()?std::move(other.value):nullptr){}/*move constr*/

	inline MaybeBase& operator=(const MaybeBase& other){/*copy assignement*/
		value=(other.full()?std::make_shared<T>(*other.value):nullptr);
		return *this;
	}
	inline MaybeBase& operator=(MaybeBase&& other){/*move assignement*/
		value=(other.full()?std::move(other.value):nullptr);
		return *this;
	}

	//! standart copy
	inline void set(const T& data){value=std::make_shared<T>(data);}
	//! better performance
	inline void set(T&& data){value=std::make_shared<T>(std::move(data));}
	//! if T copy contructor does not existe
	inline void set(std::shared_ptr<T> data){value=data;}


	inline void reset(){value=nullptr;}

	inline bool full()const{return value.operator bool();}
	inline operator bool()const{return full();}
	//! return true if the data is == or if there is no data
	bool operator ==(const MaybeBase& other)const{
		auto noData=(!value.operator bool())&&(!other.value.operator bool());
		auto sameData=(value.operator bool()&&other.value.operator bool())&&((*value)==(*other.value));
		return sameData||noData;
	}

	bool operator<(const MaybeBase& other) const{
		return value < other.value;
	}

	T& val(){
		assert(value.operator bool());
		return *value;
	}
	const T& val()const{
		assert(value.operator bool());
		return *value;
	}

	template<class Archive>
	void serialize(Archive &ar){
		ar(value);
	}

private:
	std::shared_ptr<T> value;
};

template<class T,class _ = void>
//! encapsulate what could be "a pointer that can be pointing to nothing", this is equal to c++ 17 std::optional (as we are in c++11 for the time)
struct Maybe{
};
template<class U>
Maybe<U> Maybe_from(U value){return Maybe<U>(value);}

template<class T>
struct Maybe<T,typename std::enable_if<!is_implicitly_convertible<T,bool>::value>::type> : public MaybeBase<T>{
	using base = MaybeBase<T>;
	using base::base;
};

template<class T>
struct Maybe<T,typename std::enable_if<is_implicitly_convertible<T,bool>::value>::type> : public MaybeBase<T>{
	using base = MaybeBase<T>;
	using base::base;
	operator bool()=delete ;// is_implicitly_convertible<T,bool> so use the ::full() methode insdead of the bool conversion operator
};

