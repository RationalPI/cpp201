#include <vector>
#include <iostream>
#include <tuple>
#include <map>

//! Simple Vector 3D x,y,z
template<class T>
struct Vec3{
	T x,y,z;
};

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

//! Simple Vector 3D x,y,z that can implicitly convert with other types
template<class T>
struct Vec3Convertible{
	T x,y,z;
	Vec3Convertible(){}
	template<class U>
	Vec3Convertible(const Vec3Convertible<U>& other){
		x=other.x;
		y=other.y;
		z=other.z;
	}
};

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

//! Simple iterable structure
template<class T>
struct CustomIterable{
	// https://www.cplusplus.com/reference/iterator/
	struct iteratorForForLoops{
		iteratorForForLoops(CustomIterable& owner,size_t index):owner(owner),index(index){}
		bool operator !=(const iteratorForForLoops& other){return other.index!=index;}
		iteratorForForLoops& operator ++(){
			if(*this!=owner.end()){
				index++;
			}
			return *this;
		}
		T& operator *(){return owner.data.at(index);}
	private:
		size_t index;
		CustomIterable& owner;
	};
	iteratorForForLoops begin(){
		return iteratorForForLoops(*this,0);
	}
	iteratorForForLoops end(){
		return iteratorForForLoops(*this,data.size());
	}
	void append(const T& val){
		data.push_back(val);
	}
private:
	std::vector<T> data;
};


/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

//duck typink like capabilities
template<class T>
void makeQuack(const T& duck){
	duck.quacks;
	return;
}
struct NotADuck{};
struct ADuck{int quacks;};

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

//default type, default value
template<class T=decltype ("")>
void print(const T& v=""){
	std::cout << v << std::endl;
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

//variadic template (exemple python like print function)
template<class T,class... Rest>
void print(const T& v,Rest... rest){
	std::cout << v << " ";
	print(rest...);
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

//Advanced vadriadic bonus
//! tell if the depth is the last element of a tuple
template<class T>
constexpr bool isTupleSize(int depth){
	return std::tuple_size<T>::value == depth+1;
}

//! in case the depth is the last element of a tuple
template<class T,int depth>
typename std::enable_if<isTupleSize<T>(depth),void>::type
printTuple_(const T& tuple){
	std::cout << std::get<depth>(tuple) << " ]" << std::endl;
}

//! in case the depth is not the last element of a tuple
template<class T,int depth>
typename std::enable_if<!isTupleSize<T>(depth),void>::type
printTuple_(const T& tuple){
	std::cout << std::get<depth>(tuple) << " , ";
	printTuple_<T,depth+1>(tuple);
}

template<class... T>
void printTuple(const std::tuple<T...>& tuple){
	std::cout << "tuple : [ ";
	printTuple_<std::tuple<T...>,0>(tuple);
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

namespace bonusVariadic {
// if you realy need bare bones function pointers
template<class Ret,class... Args>
struct Fptr {
	Fptr(Ret(*f)(Args...) ):f(f){}
	Ret operator ()(Args... a){return (*f)(a...);}
private:
	Ret(*f)(Args...);
};

// random exemple
template<class Ret,class... Args>
struct FptrCached {
	FptrCached(Ret(*f)(Args...) ):f(f){}
	Ret operator ()(Args... a){
		auto args=std::tuple<Args...>(a...);
		if(!cache.count(args)){
			cache[args]=(*f)(a...);
		}
		return cache[args];
	}
private:
	Ret(*f)(Args...);
	std::map<std::tuple<Args...>,Ret> cache;
};

int test(int arg){std::cout << "test bonusVariadic : " << arg << std::endl; return arg;}

}

int main(){
	/*basic template*/{
		Vec3Convertible<int> a;
		Vec3Convertible<float> b=a;// copy constructor with TEMPLATE TYPE DEDUCTION
	}

	/*iterable*/{
		CustomIterable<int> vec;
		vec.append(0);
		vec.append(42);
		vec.append(99);
		for (auto& element : vec) {
			std::cout << element << std::endl;
		}
	}

	/*duck typink like capabilities + template type deduction*/{
		makeQuack<ADuck>({});
		//makeQuack<NotADuck>({});
		makeQuack(ADuck());
		//makeQuack(NotADuck());
	}

	/*default type, default value*/{
		print();
		print(55);
		print("hello");
		print();
	}

	/*variadic template (exemple python like print function)*/{
		print("world",0,1,41.999);
	}

	/*Advanced vadriadic bonus*/{
		print();
		auto t=std::make_tuple(2,3.f,4.,"hello");
		printTuple(t);
	}

	/*bonus*/{
		int(*f1)(int) = bonusVariadic::test;
		bonusVariadic::Fptr<int,int> f2(f1);
		bonusVariadic::Fptr<int,int> f3(bonusVariadic::test);
		f1(0);
		f2(0);
		f3(0);

		bonusVariadic::FptrCached<int,int> fcached(bonusVariadic::test);
		fcached(2);
		for (int i = 0; i < 5; ++i) {
			print(fcached(999));
		}
	}

	return 0;
}
