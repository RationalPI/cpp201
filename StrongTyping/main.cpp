#include <cmath>
#include <map>

namespace standard {

	//! \brief compute the cinetic energy of an massive object
	//! \param masse Kg
	//! \param speed m/s
	float energy(float masse,float speed){
		return masse*std::pow(speed,2);
	}

	}

namespace Better {

	using SiMasse=float;
	using SiSpeed=float;
	using SiEnergy=float;

	//! \brief compute the cinetic energy of an massive object
	SiEnergy energy(SiMasse masse,SiSpeed speed){
		return masse*std::pow(speed,2);
	}

	void usage(){
		energy(1,2);

		SiMasse m=1;
		SiSpeed s=2;

		energy(m,s);
		energy(s,m);//compile and that's bad
		m==s;       //compile and that's bad
		m!=s;       //compile and that's bad
		m<s;        //compile and that's bad
	}

	}

namespace tryingBetter {

	//very tideous (could be automated by templates or MACROS)
	struct SiMasse {
		SiMasse(float value):value(value){}
		operator float(){return value;}
	private:
		float value;
	};

	//very tideous (could be automated by templates or MACROS)
	struct SiSpeed {
		SiSpeed(float value):value(value){}
		operator float(){return value;}
		float value;
	};
	//very tideous (could be automated by templates or MACROS)
	struct SiEnergy {
		SiEnergy(float value):value(value){}
		operator float(){return value;}
		float value;
	};

	//! \brief compute the cinetic energy of an massive object
	SiEnergy energy(SiMasse masse,SiSpeed speed){
		return masse*std::pow(speed,2);
	}

	void usage(){
		energy({1},{2});

		SiMasse m={1};
		SiSpeed s={2};

		energy(m,s);
		//  energy(s,m);//does not compile and that's good
		m==s;       //compile and that's bad
		m!=s;       //compile and that's bad
		m<s;        //compile and that's bad
	}

	}

namespace tryingBetterAgain {

	template<class T>
	struct StrongType{
		StrongType(T value):value(value){}
		//we whant to be able to use StrongTypes as key in maps
		bool operator ==(const StrongType<T>& other) const {return value==other.value;}
		//we whant to be able to use StrongTypes as key in maps
		bool operator !=(const StrongType<T>& other) const {return value!=other.value;}
		//we whant to be able to use StrongTypes as key in maps
		bool operator <(const StrongType<T>& other) const {return value<other.value;}
		T& val(){return value;}
	private:
		T value;
	};

	struct SiMasse : public StrongType<float>{using StrongType::StrongType;};
	struct SiSpeed : public StrongType<float>{using StrongType::StrongType;};
	struct SiEnergy: public StrongType<float>{using StrongType::StrongType;};
	//! \brief compute the cinetic energy of an massive object
	SiEnergy energy(SiMasse masse,SiSpeed speed){
		return masse.val()*std::pow(speed.val(),2);
	}

	void usage(){
		energy({1},{2});

		SiMasse m={1};
		SiSpeed s={2};

		energy(m,s);
		//  energy(s,m);//does not compile and that's good
		m==s;       //compile and that's bad
		m!=s;       //compile and that's bad
		m<s;        //compile and that's bad
	}

	}

namespace Solution {

	template<class T,class tag>
	struct StrongType{
		StrongType(){}
		StrongType(T value):value(value){}
		bool operator ==(const StrongType<T,tag>& other) const {return value==other.value;}
		bool operator !=(const StrongType<T,tag>& other) const {return value!=other.value;}
		bool operator <(const StrongType<T,tag>& other) const {return value<other.value;}
		T& val(){return value;}

		//! deleted between any strong_type conversion
		template <typename T2,typename tag2>
		explicit StrongType(StrongType< T2, tag2> const&)=delete;
	private:
		T value;
	};

	struct ExempleTag;
	struct Exemple : public StrongType<float,ExempleTag>{using StrongType::StrongType;};

#define TAGED(val)val ## _TypeFlag
#define strong_type(name, T )\
	struct TAGED(name){};\
	struct name : public StrongType<T,TAGED(name)>{using StrongType::StrongType;};

	// I DO NOT RECOMEND IMPLEMENTING YOUR OWN UNIT SYSTEM
	// use a well established and tested framworks for that

	//! Kg
	strong_type(SiMasse,float)
	//! m/s
	strong_type(SiSpeed,float)
	//! Joules
	strong_type(SiEnergy,float)

	//! \brief compute the cinetic energy of an massive object
	SiEnergy energy(SiMasse masse,SiSpeed speed){
		return masse.val()*std::pow(speed.val(),2);
	}

	void usage(){
		energy({1},{2});

		SiMasse m={1};
		SiSpeed s={2};

		energy(m,s);
		//  energy(s,m);//does not compile and that's good
		//  m==s;       //does not compile and that's good
		//  m!=s;       //does not compile and that's good
		//  m<s;        //does not compile and that's good
	}

	namespace advanced {

		//! KgM/s
		strong_type(SiMasseSpeed,float)

		SiMasseSpeed operator *(SiMasse a,SiSpeed b){return a.val()*b.val();}
		SiEnergy operator *(SiMasseSpeed a,SiSpeed b){return a.val()*b.val();}

		//! \brief compute the cinetic energy of an massive object
		SiEnergy advancedEnergy(SiMasse masse,SiSpeed speed){
			return masse*speed*speed;
		}

		void typeTest(){
			static_assert (std::is_same<
					decltype (SiMasse()*SiSpeed()*SiSpeed()),
					SiEnergy
					>::value,"same type");
		}

		namespace advancedPP {

			//! speed in Km/h
			strong_type(KMH_Base,float);
			//! speed in Km/h
			struct Kmh : public KMH_Base {
				using KMH_Base::KMH_Base;
				Kmh(SiSpeed s):KMH_Base(s.val()*3.6){}
				operator SiSpeed(){return val()/3.6;}
			};


			//! \brief compute the cinetic energy of an massive object
			SiEnergy energy(SiMasse masse,Kmh speed){
				return masse*speed*speed;
			}

			}

		}

	}

int main(){
	return 0;
}
