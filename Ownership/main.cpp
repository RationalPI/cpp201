//capture dans une lambda

#include "strongtype.h"
#include <set>
#include <map>


namespace Args {
	void f3(const int& arg){}
	/*equivalent in certain cases but never better =>*/ void f1(int arg){}
	void f2(int& arg){}
	/*equivalent but never better =>*/ void f4(std::shared_ptr<int> arg){}

	// a reference should never be trusted outside the scope from were it came
	}

namespace DataModels{

	//! n depth ownership impossible tideous/impossible to know how own the data
	//! (need to look at all instances of mkSHared)
	struct BadCar{
		struct Compromisable{
			bool compromised=false;
		};
		struct Bolt:public Compromisable{
		};
		struct Wheel:public Compromisable{
			std::set<std::shared_ptr<Bolt>> bolts;
		};
		struct Engine:public Compromisable{
			std::set<std::shared_ptr<Bolt>> bolts;
		};
		struct FrontTrain:public Compromisable{
			std::set<std::shared_ptr<Bolt>> bolts;
			std::set<Wheel*> wheels;
		};
		struct RearTrain:public Compromisable{
			std::set<std::shared_ptr<Bolt>> bolts;
			std::set<std::shared_ptr<Wheel>> wheels;
		};

		std::set<std::shared_ptr<Bolt>> all_bolts;
		std::set<std::shared_ptr<Wheel>> all_wheels;

		Engine engine;
		FrontTrain frontTrain;
		RearTrain rearTrain;

		bool compromized(){
			//
			return true;
		}
	};

	//! Zero depth reference per type ID clarify ownership
	struct BetterCar{
		strong_type(WheelID,int,NO_EXPORT)
		strong_type(BoltID,int,NO_EXPORT)
		struct Compromisable{
			bool compromised=false;
		};
		struct Wheel:public Compromisable{
			WheelID idInCar;
			std::set<BoltID> BoltIDsInCar;
		};
		struct Bolt:public Compromisable{
			BoltID idInCar;
		};
		struct Engine:public Compromisable{
			std::set<BoltID> BoltIDsInCar;
		};
		struct FrontTrain:public Compromisable{
			std::set<BoltID> BoltIDsInCar;
			std::set<WheelID> wheelIDsInCar;
		};
		struct RearTrain:public Compromisable{
			std::set<BoltID> BoltIDsInCar;
			std::set<WheelID> wheelIDsInCar;
		};

		std::map<WheelID,Wheel> wheels;
		std::map<BoltID,Wheel> bolts;
		Engine engine;
		FrontTrain frontTrain;
		RearTrain rearTrain;

		bool compromized(){
			for (auto& wheel:wheels) {
				if(wheel.second.compromised)return true;
			}
			for (auto& bolt:bolts) {
				if(bolt.second.compromised)return true;
			}
			if(engine.compromised)return true;
			if(frontTrain.compromised)return true;
			if(rearTrain.compromised)return true;
			return false;
		}
	};

	}

int main(){
	return 0;
}
